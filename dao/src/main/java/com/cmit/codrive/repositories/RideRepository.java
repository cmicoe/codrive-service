package com.cmit.codrive.repositories;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cmit.codrive.entities.Ride;

@Repository
public interface RideRepository extends JpaRepository<Ride, Long>, JpaSpecificationExecutor<Ride>, Serializable {

	@Query(value = "select * from Ride r where r.id IN ( select rp.rideId from RidePartner rp  "
			+ " where rp.partnerId = :userId and isCancelled = 0 and isCompleted= 0) and r.rideStatusId =:status order by r.startDateTime", 
			countQuery = "select count(*) from Ride r where r.id IN ( select rp.rideId from RidePartner rp "
					+ " where rp.partnerId = :userId and isCancelled = 0 and isCompleted= 0) and r.rideStatusId =:status order by r.startDateTime",
			nativeQuery = true)
	public Page<Ride> findAllRidesForUserByStatus(@Param(value = "userId") final Long userId,
			@Param(value = "status") final Long status, final Pageable pageable);

	@Query(

			value = " select * from Ride r " 
					+ " where routeId IN (" 
								+ " select r2.id from Route r2 "
								+ " INNER JOIN PickupLocation p ON r2.id = p.routeId " 
								+ " where " + " r2.riderId <> :userId"
								+ " AND ( " 
											+ " r2.startDate < :startDate OR r2.endDate > :endDate" 	
								+ "			OR ( r2.startDate BETWEEN :startDate AND :endDate) " 
							    +       ") "
								+ " AND "
								+ " ( "
										+ " 2 = ( "
													+ " select count(*)	from PickupLocation pl where pl.routeId = r2.id "  
													+ "AND ( "
															+ "( LOWER(pl.location) like CONCAT('%',:source ,'%') ) "		
															+ " OR (LOWER(pl.location) like  CONCAT('%', :destination ,'%') )"
													   + " )"
										   +   " ) " 
								+ " ) "
					
					+ ") "
					+ " AND 0 = (select count(*) from RidePartner rp where rp.rideId = r.id AND rp.partnerId = :userId)"
					+ " AND r.startDateTime BETWEEN :startDate AND :endDate"
					+ " AND r.rideStatusId = 1 "
					+ " AND r.availableSeats > 0 "
					+ " AND DAYOFWEEK(startDateTime) IN :daysOfWeek",
					
			countQuery = "select count(*) from Ride r " + " where routeId IN (" + " select r2.id from Route r2 "
					+ " INNER JOIN PickupLocation p ON r2.id = p.routeId " + " where " + " r2.riderId <> :userId"
					+ " AND ( r2.startDate < :startDate OR r2.endDate > :endDate" 	
					+ "			OR ( r2.startDate BETWEEN :startDate AND :endDate) ) "

					+ " AND "
				
					
					+ " ( "
					+ " 2 = ( "
					+ " select count(*)	from PickupLocation pl where pl.routeId = r2.id "  
					+ "AND ( "
					+ "( "
					+ " LOWER(pl.location) like CONCAT('%',:source ,'%') ) "		
					+ " OR (LOWER(pl.location) like  CONCAT('%', :destination ,'%') )"
					+ " )"
					
					+	" ) " 
					+	" ) "
					
					+ ") " 
					+ " AND 0 = (select count(*) from RidePartner rp where rp.rideId = r.id AND rp.partnerId = :userId)"
					+ " AND r.startDateTime BETWEEN :startDate AND :endDate"
					+ " AND r.rideStatusId = 1 "
					+ " AND r.availableSeats > 0 "
					+ " AND DAYOFWEEK(startDateTime) IN :daysOfWeek",

			nativeQuery = true)
	public Page<Ride> searchRides(@Param(value = "startDate") final LocalDateTime startDate,
			@Param(value = "endDate") final LocalDateTime endDate, 
			@Param(value = "source") final String source,
			@Param(value = "destination") final String destination,
			@Param(value = "daysOfWeek") final List<String> daysOfWeek, @Param(value = "userId") final Long userId,
			final Pageable pageable);
	
	@Query("select r from Ride r where r.id= :id AND r.rideStatus= 1")
	Optional<Ride> findUpcomingRideById(@Param(value="id") final Long id);
	
	@Query(
			value= "SELECT * FROM Ride r WHERE r.rideStatusId = 3 AND r.startDateTime BETWEEN :startDate AND :endDate",
			countQuery = "SELECT count(*) FROM Ride r WHERE r.rideStatusId = 3 AND r.startDateTime BETWEEN :startDate AND :endDate",
			nativeQuery = true
			)
	Page<Ride> findCompletedRideWithinPeriod(@Param(value="startDate") LocalDate startDate, @Param(value="endDate") LocalDate endDate,
			final Pageable pageable);
}
	