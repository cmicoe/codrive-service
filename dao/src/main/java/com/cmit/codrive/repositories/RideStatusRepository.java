package com.cmit.codrive.repositories;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmit.codrive.entities.RideStatus;

public interface RideStatusRepository extends JpaRepository<RideStatus, Long>, JpaSpecificationExecutor<RideStatus>, Serializable {

	Optional<RideStatus> findByCode(String statusCode);
}
