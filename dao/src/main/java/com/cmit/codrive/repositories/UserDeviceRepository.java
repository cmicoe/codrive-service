package com.cmit.codrive.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cmit.codrive.entities.UserDevice;

@Repository
public interface UserDeviceRepository
		extends JpaRepository<UserDevice, Long>, JpaSpecificationExecutor<UserDevice>, Serializable {

	public Optional<UserDevice> findByUserIdAndUuid(Long userId, String uuid);
	public List<UserDevice> findByUserId(Long userId);
}
