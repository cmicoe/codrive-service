/*
Created: 25-05-2020
Modified: 26-05-2020
Model: MySQL 8.0
Database: MySQL 8.0
*/

-- Trigger to generate the rides for each new Route -------------------------------------------------

DELIMITER $$
CREATE TRIGGER after_route_insert
AFTER INSERT
ON Route FOR EACH ROW
BEGIN
 DECLARE temp DATE DEFAULT NEW.startDate;
 WHILE temp <= NEW.endDate DO
 
   IF LOCATE(DAYOFWEEK(temp) , NEW.daysInWeek ) >0 THEN
	        INSERT INTO Ride(routeId, startDateTime, rideStatusId, availableSeats, createdBy, modifiedBy, createdTimestamp, modifiedTimestamp)
            VALUES (NEW.id, timestamp(temp, NEW.startTime), (SELECT id from RideStatus WHERE code='NEW'), NEW.offeringSeats, NEW.createdBy, NEW.modifiedBy, NEW.createdTimestamp, NEW.modifiedTimestamp );
	END IF;
     SET temp = DATE_ADD(temp,INTERVAL 1 day);
    END WHILE;
END$$

-- Trigger to create the entry in RidePartner for each new Ride -------------------------------------------------

DELIMITER $$
CREATE TRIGGER after_ride_insert
AFTER INSERT
ON Ride FOR EACH ROW
BEGIN
	INSERT INTO RidePartner(rideId, partnerId, isRider, createdBy, modifiedBy, createdTimestamp, modifiedTimestamp)
	VALUES (NEW.id, (SELECT riderId FROM Route WHERE id = (SELECT routeId FROM Ride WHERE id = NEW.id ) ), 1, NEW.createdBy, 
				NEW.modifiedBy, NEW.createdTimestamp, NEW.modifiedTimestamp );
END$$