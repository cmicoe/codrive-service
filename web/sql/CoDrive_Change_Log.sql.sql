-- 26/05/2020 SANJAY : Insert script for default testuser and RideStatus table

INSERT INTO User(firstname, lastname, email, mobile) VALUES ('Test', 'User', 'testuser@gmail.com', '9874561230');
INSERT INTO RideStatus(code, description, createdBy, modifiedBy) 
	VALUES ('NEW', 'New', (select id from User where email= 'testuser@gmail.com'), 
		                            (select id from User where email= 'testuser@gmail.com'));
INSERT INTO RideStatus(code, description, createdBy, modifiedBy) 
	VALUES ('ONGOING', 'Ongoing', (select id from User where email= 'testuser@gmail.com'), 
		                            (select id from User where email= 'testuser@gmail.com'));														
INSERT INTO RideStatus(code, description, createdBy, modifiedBy) 
	VALUES ('COMPLETED', 'Completed', (select id from User where email= 'testuser@gmail.com'), 
		                            (select id from User where email= 'testuser@gmail.com'));
INSERT INTO RideStatus(code, description, createdBy, modifiedBy) 
	VALUES ('CANCELLED', 'Completed', (select id from User where email= 'testuser@gmail.com'), 
		                            (select id from User where email= 'testuser@gmail.com'));
									
-- 28/05/2020 MAYURESH : Table alter script
									
ALTER TABLE Route MODIFY origin varchar(500);
ALTER TABLE Route MODIFY destination varchar(500);
ALTER TABLE PickupLocation MODIFY location varchar(500);
ALTER TABLE User ADD column gender varchar(10);
ALTER TABLE Ride ADD COLUMN availableSeats Int NOT NULL default 0;
ALTER TABLE PickupLocation ADD COLUMN sequenceNo int NOT NULL default 0;

-- 03/06/2020 SANJAY: TABLE UserDevice

CREATE TABLE `UserDevice`
(
  `id` Int NOT NULL AUTO_INCREMENT,
  `userId` Int NOT NULL,
  `token` Varchar(500) NOT NULL,
  `appVersion` Varchar(20) NOT NULL,
  `model` Varchar(20) NOT NULL,
  `platform` Varchar(20) NOT NULL,
  `uuid` Varchar(50) NOT NULL,
  `manufacturer` Varchar(20) NOT NULL,
  `osVersion` Varchar(20) NOT NULL,
  `isVirtual` Tinyint(1) NOT NULL DEFAULT 0,
  `createdBy` Int NOT NULL,
  `createdTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `modifiedBy` Int NOT NULL,
  `modifiedTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
);

CREATE INDEX `IX_Relationship40` ON `UserDevice` (`userId`);

CREATE INDEX `IX_Relationship41` ON `UserDevice` (`createdBy`);

CREATE INDEX `IX_Relationship42` ON `UserDevice` (`modifiedBy`);

CREATE UNIQUE INDEX `AK_UserDevice_UserId_UUID` ON `UserDevice` (`userId`, `uuid`);

ALTER TABLE `UserDevice` ADD CONSTRAINT `Relationship40` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `UserDevice` ADD CONSTRAINT `Relationship41` FOREIGN KEY (`createdBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `UserDevice` ADD CONSTRAINT `Relationship42` FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


-- 06/06/2020 SANJAY : RidePartner Table Alter Script
									
ALTER TABLE RidePartner ADD COLUMN pickupLocationId Int NULL;
ALTER TABLE RidePartner ADD COLUMN dropLocationId Int NULL;
ALTER TABLE RidePartner ADD COLUMN cancelledBy Int NULL;
ALTER TABLE RidePartner ADD COLUMN isCancelled Tinyint(1) NOT NULL DEFAULT 0;
ALTER TABLE RidePartner ADD COLUMN isCompleted Tinyint(1) NOT NULL DEFAULT 0;

CREATE INDEX `IX_RP_pickupLocationId` ON `RidePartner` (`pickupLocationId`);
CREATE INDEX `IX_RP_dropLocationId` ON `RidePartner` (`dropLocationId`);
CREATE INDEX `IX_RP_cancelledBy` ON `RidePartner` (`cancelledBy`);

ALTER TABLE `RidePartner` ADD CONSTRAINT `FK_RP_PL_pickupLocationId` FOREIGN KEY (`pickupLocationId`) 
	REFERENCES `PickupLocation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
	
ALTER TABLE `RidePartner` ADD CONSTRAINT `FK_RP_PL_dropLocationId` FOREIGN KEY (`dropLocationId`) 
	REFERENCES `PickupLocation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
	
ALTER TABLE `RidePartner` ADD CONSTRAINT `FK_RP_U_cancelledBy` FOREIGN KEY (`cancelledBy`) 
	REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
	
CREATE UNIQUE INDEX `AK_RP_rideId_partnerId` ON `RidePartner` (`rideId`, `partnerId`);

