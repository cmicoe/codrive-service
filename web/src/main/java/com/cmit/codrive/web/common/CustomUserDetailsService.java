package com.cmit.codrive.web.common;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.cmit.codrive.entities.SecurityUser;
import com.cmit.codrive.entities.User;
import com.cmit.codrive.repositories.UserRepository;

@Component
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		Optional<User> result = userRepository.findByEmail(username);

		if (result.isPresent())
		{
			return new SecurityUser(result.get()); 
		}

		throw new UsernameNotFoundException("Invalid username");
	}


}
