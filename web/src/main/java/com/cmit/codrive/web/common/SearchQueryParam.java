package com.cmit.codrive.web.common;

import java.time.LocalDateTime;
import java.time.LocalTime;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
public class SearchQueryParam {

	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private LocalTime startTime;
	private LocalTime endTime;
	private String source;
	private String destination;
	private String daysOfWeek = "1,2,3,4,5,6,7";
	
	public SearchQueryParam(LocalDateTime startDate, LocalDateTime endDate,
			LocalTime startTime, LocalTime endTime, String source,
			String destination, String daysOfWeek) {
		this.startDate = startDate;
		this.endDate = endDate;
		
		String hour = String.format("%02d", this.startDate.getHour());
		String min = String.format("%02d", this.startDate.getMinute());
		this.startTime = LocalTime.parse(hour + ":" + min);
		this.endTime = LocalTime.parse("23:59");
		this.source = source;
		this.destination = destination;
		this.daysOfWeek = daysOfWeek != null ? daysOfWeek : "1,2,3,4,5,6,7";
	}
}
