package com.cmit.codrive.web.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cmit.codrive.services.route.RouteService;
import com.cmit.codrive.valueobjects.RouteVO;
import com.cmit.codrive.web.common.BaseController;
import com.cmit.codrive.web.common.CommonResponse;

@RestController
@CrossOrigin
@RequestMapping(value = "routes")
public class RouteController extends BaseController {
	
	@Autowired
	RouteService routeService;
	
	@Autowired
	ModelMapper mapper;
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse createRoute(@RequestBody RouteVO request) {
		CommonResponse response = new CommonResponse();
		try {
			final RouteVO result = this.routeService.createEntity(getUserContext(), request);
			response.setStatus(true);
			response.setData(result);
		} catch(Exception e) {
			if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}
		}
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getAllRoutes() {
		CommonResponse response = new CommonResponse();
		
		try {
			List<RouteVO> routes = this.routeService.getEntityList(getUserContext());
			response.setData(routes);
				
		} catch(Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}
		}
		
		return response;
	}

	@RequestMapping(value= "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getRouteById(@PathVariable("id") Long id) {
		CommonResponse response = new CommonResponse();
		
		try {
			RouteVO result = this.routeService.getEntity(getUserContext(), id);
			response.setData(result);
				
		} catch(Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}
		}
		
		return response;
	}
	
	@RequestMapping(value= "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public CommonResponse deleteRouteById(@PathVariable("id") Long id) {
		CommonResponse response = new CommonResponse();
		
		try {
			this.routeService.deleteEntity(getUserContext(), id);
				
		} catch(Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}
		}
		
		return response;
	}
	
	@RequestMapping(value= "/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public CommonResponse updateRouteById(@RequestBody RouteVO request) {
		CommonResponse response = new CommonResponse();
		
		try {
			final RouteVO result = this.routeService.updateEntity(getUserContext(), request);
			response.setData(result);	
		} catch(Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}
		
		return response;
	}
	
	
}
