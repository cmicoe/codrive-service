package com.cmit.codrive.web.common;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
@NoArgsConstructor
public class AuthenticationResponse {

	private String token;
	
	private String email;
}
