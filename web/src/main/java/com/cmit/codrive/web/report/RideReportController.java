package com.cmit.codrive.web.report;

import java.time.LocalDate;
import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cmit.codrive.services.report.RideReportService;
import com.cmit.codrive.valueobjects.RideVO;
import com.cmit.codrive.web.common.BaseController;
import com.cmit.codrive.web.common.CommonResponse;

@RestController
@CrossOrigin
@RequestMapping(value = "report")
public class RideReportController extends BaseController {

	@Autowired
	private RideReportService rideService;
	
	@RequestMapping(value = "/completed", method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getCompletedRideReport(final @RequestParam("startDate") @DateTimeFormat(iso = ISO.DATE) LocalDate startDate, 
			final @RequestParam("endDate")  @DateTimeFormat(iso = ISO.DATE) LocalDate endDate,
			final @RequestParam("page") Integer page, final @RequestParam("size") Integer size) {
		CommonResponse response = new CommonResponse();

		try {
			Page<RideVO> rides = this.rideService.getCompletedRidesForReport(getUserContext(), startDate, endDate, page, size);
			response.setData(rides);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No ride found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}
		return response;
	}

}
