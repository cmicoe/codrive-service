package com.cmit.codrive.web.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cmit.codrive.services.userdevice.UserDeviceService;
import com.cmit.codrive.valueobjects.UserDeviceVO;
import com.cmit.codrive.web.common.BaseController;
import com.cmit.codrive.web.common.CommonResponse;

@RestController
@CrossOrigin
@RequestMapping(value = "device")
public class UserDeviceController extends BaseController {

	@Autowired
	private UserDeviceService userDeviceService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse registerDevice(final @RequestBody UserDeviceVO userDeviceVO) {
		CommonResponse response = new CommonResponse();

		try {
			this.userDeviceService.createOrUpdateEntity(getUserContext(), userDeviceVO);
			response.setStatus(true);
			response.setData("Device registered successfully");
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException
					|| e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user device found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}

	@RequestMapping(value = "/refreshToken", method = RequestMethod.PUT)
	@ResponseBody
	public CommonResponse refreshToken(final @RequestBody UserDeviceVO userDeviceVO) {
		CommonResponse response = new CommonResponse();

		try {
			this.userDeviceService.updateEntity(getUserContext(), userDeviceVO.getUuid(), userDeviceVO.getToken());
			response.setStatus(true);
			response.setData("Device token refreshed successfully");
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException
					|| e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user device found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}

	@RequestMapping(value = "/deregister", method = RequestMethod.DELETE)
	@ResponseBody
	public CommonResponse deregisterDevice(final @RequestBody UserDeviceVO userDeviceVO) {
		CommonResponse response = new CommonResponse();

		try {
			this.userDeviceService.deleteEntity(getUserContext(), userDeviceVO.getUuid());
			response.setStatus(true);
			response.setData("Device deregistered successfully");
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException
					|| e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user device found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}

	@RequestMapping(value = "/my", method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getUserRegisteredDevices() {
		CommonResponse response = new CommonResponse();

		try {
			List<UserDeviceVO> devices = this.userDeviceService.getEntityList(getUserContext());
			response.setStatus(true);
			response.setData(devices);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException
					|| e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user device found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}
}
