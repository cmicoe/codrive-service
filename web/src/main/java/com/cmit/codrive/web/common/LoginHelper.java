package com.cmit.codrive.web.common;

import java.nio.charset.StandardCharsets;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cmit.codrive.valueobjects.UserVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class LoginHelper {

	@Autowired
	private ObjectMapper objectMapper;
	
	@Value("${webHR_api_url}")
	private String webHRApiUrl;
	
	public UserVO getUserDetails(String empId, String token) throws Exception {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(this.webHRApiUrl + "empdata?empid=" + empId);
		httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);
		CloseableHttpResponse httpResponse = client.execute(httpGet);

		String responseBody = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
		final UserVO userValueObject = getUserValueObject(responseBody);

		client.close();
		httpResponse.close();

		return userValueObject;
	}

	private UserVO getUserValueObject(String responseBody) throws JsonMappingException, JsonProcessingException {

		JsonNode jsonNode = objectMapper.readTree(responseBody);

		String empId = jsonNode != null ? jsonNode.get("data").get("empid").asText() : null;
		String firstName = jsonNode != null ? jsonNode.get("data").get("firstName").asText() : null;
		String middleName = jsonNode != null ? jsonNode.get("data").get("middleName").asText() : null;
		String lastName = jsonNode != null ? jsonNode.get("data").get("lastName").asText() : null;
		String email = jsonNode != null ? jsonNode.get("data").get("emailId").asText() : null;
		String mobile = jsonNode != null ? jsonNode.get("data").get("mobile").asText() : null;

		final UserVO userVO = new UserVO();
		userVO.setExternalSystemId(empId);
		userVO.setFirstName(firstName);
		userVO.setMiddleName(middleName);
		userVO.setLastName(lastName);
		userVO.setEmail(email);
		userVO.setMobile(mobile);

		return userVO;
	}

	public String authenticateRequest(AuthenticationRequest request) throws Exception {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(this.webHRApiUrl + "login");

		ObjectMapper objectMapper = new ObjectMapper();
		String requestString = objectMapper.writeValueAsString(request);
		StringEntity entity = new StringEntity(requestString, ContentType.APPLICATION_JSON);
		httpPost.setEntity(entity);
		CloseableHttpResponse httpResponse = client.execute(httpPost);

		String responseBody = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
		client.close();
		httpResponse.close();

		return responseBody;
	}

}
