package com.cmit.codrive.web.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cmit.codrive.services.common.RideStatusConstant;
import com.cmit.codrive.services.ride.RideService;
import com.cmit.codrive.valueobjects.RideVO;
import com.cmit.codrive.web.common.BaseController;
import com.cmit.codrive.web.common.CommonResponse;
import com.cmit.codrive.web.common.SearchQueryParam;

@RestController
@CrossOrigin
@RequestMapping(value = "rides")
public class RideController extends BaseController {

	@Autowired
	private RideService service;
	
	@RequestMapping(value = "/upcoming", method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getUpcomingRides(final @RequestParam("page") Integer page,
			final @RequestParam("size") Integer size) {
		CommonResponse response = new CommonResponse();

		try {
			Page<RideVO> rides = this.service.getUpcomingRidesForUser(getUserContext(), page, size);
			response.setData(rides);

		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}

	@RequestMapping(value = "/completed", method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getCompletedRides(final @RequestParam("page") Integer page,
			final @RequestParam("size") Integer size) {
		CommonResponse response = new CommonResponse();

		try {
			Page<RideVO> rides = this.service.getCompletedRidesForUser(getUserContext(), page, size);
			response.setData(rides);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No ride found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse searchRides(final @RequestBody SearchQueryParam searchParam,
			final @RequestParam("page") Integer page, final @RequestParam("size") Integer size) {
		final CommonResponse response = new CommonResponse();

		try {
			String[] daysofWeek = searchParam.getDaysOfWeek().split(",");
			List<String> selectedDays = Arrays.asList(daysofWeek);
			
			final Page<RideVO> rides = this.service.searchRides(getUserContext(), searchParam.getStartDate(),
					searchParam.getEndDate(), searchParam.getSource(), searchParam.getDestination(), selectedDays, page, size);
			response.setData(rides);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}
	
	@RequestMapping(value= "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public CommonResponse deleteRideById(@PathVariable("id") Long id) {
		CommonResponse response = new CommonResponse();
		
		try {
			this.service.deleteEntity(getUserContext(), id);
				
		} catch(Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}
		
		return response;
	}

	@RequestMapping(value= "/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public CommonResponse updateRideStatus(@PathVariable("id") Long id, final @RequestParam("status") String status) {
		CommonResponse response = new CommonResponse();
		
		try {
			
			if (RideStatusConstant.CANCELLED.toString().equals(status)) {
				this.service.cancelRide(getUserContext(), id);				
			} else if (RideStatusConstant.COMPLETED.toString().equals(status)) {
				this.service.completeRide(getUserContext(), id);
			}
				
		} catch(Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride found for given id.", e);
			} else if (e instanceof IllegalArgumentException || e instanceof IllegalStateException) {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is not allowed to perform this action", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}
		
		return response;
	}
	
}
