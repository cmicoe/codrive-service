package com.cmit.codrive.web.common;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
@NoArgsConstructor
public class CommonResponse implements Serializable {

	private static final long serialVersionUID = -1704381153615240996L;
	private boolean status = true;
	private Integer errorCode;
	private String errorMessage;	
	private Object data;

}
