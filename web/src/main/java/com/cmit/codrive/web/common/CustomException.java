package com.cmit.codrive.web.common;

public class CustomException extends Exception {

	private static final long serialVersionUID = -4389262873133548500L;

	public CustomException(String errorCcode, String errorMessage) {
	        super(errorMessage);
    }
}
