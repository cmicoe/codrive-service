package com.cmit.codrive.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cmit.codrive.services.user.UserService;
import com.cmit.codrive.valueobjects.UserVO;
import com.cmit.codrive.web.common.AuthenticationRequest;
import com.cmit.codrive.web.common.AuthenticationResponse;
import com.cmit.codrive.web.common.BaseController;
import com.cmit.codrive.web.common.CommonResponse;
import com.cmit.codrive.web.common.CustomUserDetailsService;
import com.cmit.codrive.web.common.LoginHelper;
import com.cmit.codrive.web.security.JwtUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin
public class LoginController extends BaseController {

	@Autowired
	UserService userService;

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private LoginHelper helper;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse login(@RequestBody AuthenticationRequest request) {
		CommonResponse response = new CommonResponse();
		String token = null;
		String empId = null;

		// 1. Get the webHR token by authenticating user
		try {
			String webHrResponse = this.helper.authenticateRequest(request);
			JsonNode jsonNode = objectMapper.readTree(webHrResponse);

			token = jsonNode != null && jsonNode.get("data") != null ? jsonNode.get("data").get("token").asText() : null;
			empId = jsonNode != null && jsonNode.get("data") !=null ? jsonNode.get("data").get("empid").asText() : null;

			if (token == null || (token != null && token.split("\\.").length != 3)) {
				throw new IllegalAccessException("Invalid credentials");
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else if (e instanceof IllegalAccessException) {
				throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong. Please try again later.", e);
			}
		}

		// 2. Get User details from WebHR api
		UserVO userVO = null;
		try {
			userVO = this.helper.getUserDetails(empId, token);
			this.userService.createEntity(null, userVO);
		} catch (Exception e) {
			e.printStackTrace();
			
			if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else if (e instanceof IllegalAccessException) {
				throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error while saving the user details.", e);
			}

		}

		// 3. Generate JWT token
		try {
			final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());

			final String jwt = jwtUtil.generateToken(userDetails);
			AuthenticationResponse authResponse = new AuthenticationResponse();
			authResponse.setToken(jwt);
			authResponse.setEmail(userDetails.getUsername());
			response.setData(authResponse);

		} catch (Exception e) {
			e.printStackTrace();
			
			if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else if (e instanceof IllegalAccessException) {
				throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong. Please try again later.", e);
			}

		}

		return response;
	}

}