package com.cmit.codrive.web.controllers;

import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cmit.codrive.services.ridepartner.RidePartnerService;
import com.cmit.codrive.valueobjects.RidePartnerVO;
import com.cmit.codrive.web.common.BaseController;
import com.cmit.codrive.web.common.CommonResponse;

@RestController
@CrossOrigin
@RequestMapping(value = "ride-partners")
public class RidePartnerController extends BaseController {

	@Autowired
	private RidePartnerService ridePartnerService;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse addRidePartner(final @RequestBody RidePartnerVO ridePartnerVO) {
		final CommonResponse response = new CommonResponse();

		try {
			this.ridePartnerService.createEntity(getUserContext(), ridePartnerVO);
			response.setData("Success");
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException || e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride partner found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}
	
	@RequestMapping(value= "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public CommonResponse removeRidePartner(@PathVariable("id") Long id) {
		final CommonResponse response = new CommonResponse();

		try {
			this.ridePartnerService.deleteEntity(getUserContext(), id);
			response.setData("Success");
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride partner found for given id.", e);
			} else if (e instanceof NullPointerException || e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}
	
	@RequestMapping(value= "/cancel/{id}", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse cancelRidePartner(@PathVariable("id") Long id) {
		final CommonResponse response = new CommonResponse();

		try {
			this.ridePartnerService.cancelRidePartner(getUserContext(), id);
			response.setData("Success");
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride partner found for given id.", e);
			} else if (e instanceof NullPointerException || e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}
	
	@RequestMapping(value= "/complete/{id}", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse completeRidePartner(@PathVariable("id") Long id) {
		final CommonResponse response = new CommonResponse();

		try {
			this.ridePartnerService.completeRidePartner(getUserContext(), id);
			response.setData("Success");
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride partner found for given id.", e);
			} else if (e instanceof NullPointerException || e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}

		return response;
	}
	
	
	@RequestMapping(value= "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getRidePartnerById(@PathVariable("id") Long id) {
		CommonResponse response = new CommonResponse();
		
		try {
			RidePartnerVO result = this.ridePartnerService.getEntity(getUserContext(), id);
			response.setData(result);
				
		} catch(Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchElementException || e instanceof EntityNotFoundException || e instanceof IllegalArgumentException) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No ride partner found for given id.", e);
			} else if (e instanceof NullPointerException) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
			} else {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}

		}
		
		return response;
	}
	
	

}
