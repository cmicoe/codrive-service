package com.cmit.codrive.valueobjects;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
public class UserVO extends BaseEntityVO {

	private static final long serialVersionUID = 985933905634392514L;
	
	private String firstName;
	
	private String lastName;
	
	private String middleName;
	
	private String email;
	
	private String mobile;
	
	private String gender;
	
	private Boolean isActive = true;
	
	private String externalSystemId;
	
}
