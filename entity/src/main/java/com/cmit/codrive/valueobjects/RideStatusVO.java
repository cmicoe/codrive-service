package com.cmit.codrive.valueobjects;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;


@Getter(value = AccessLevel.PUBLIC)
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
property = "id")

public class RideStatusVO extends BaseEntityVO {

	private static final long serialVersionUID = -3349070292195486905L;

	private String code;
	
	private String description;
}
