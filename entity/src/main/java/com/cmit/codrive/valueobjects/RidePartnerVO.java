package com.cmit.codrive.valueobjects;

import com.cmit.codrive.entities.User;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;


@Getter(value = AccessLevel.PUBLIC)
@Setter
public class RidePartnerVO extends BaseEntityVO {

	private static final long serialVersionUID = 334612106118828082L;

	//@JsonBackReference
	private RideVO ride;
	
	private UserVO partner;
	
	private Boolean isRider = false;
	
	private PickupLocationVO pickupLocation;
	
	private PickupLocationVO dropLocation;
	
	private Boolean isCancelled = false;
	
	private User cancelledBy;

	private Boolean isCompleted = false;
}
