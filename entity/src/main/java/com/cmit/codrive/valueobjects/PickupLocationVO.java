package com.cmit.codrive.valueobjects;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
public class PickupLocationVO extends BaseEntityVO {

	private static final long serialVersionUID = -4505807903166329760L;

	private String location;

	private String longitude;

	private String lattitude;
	
	private Integer sequenceNo;
	
}
