package com.cmit.codrive.valueobjects;

import com.cmit.codrive.entities.User;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
public class UserDeviceVO  extends BaseEntityVO{

	private static final long serialVersionUID = 1789434456149632547L;

	private String token;
	
	private String appVersion;
	
	private String model;
	
	private String platform;
	
	private String uuid;
	
	private String manufacturer;
	
	private String osVersion;
	
	private User user;
	
	private Boolean isVirtual = false;
}
