package com.cmit.codrive.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name="RideStatus")
@Getter(value = AccessLevel.PUBLIC)
@Setter
@NoArgsConstructor
public class RideStatus extends BaseEntity {

	private static final long serialVersionUID = -8378086262617477134L;

	@Column(name="code")
	private String code;
	
	@Column(name="description")
	private String description;
	
}
