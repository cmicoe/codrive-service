package com.cmit.codrive.entities;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;


public class UserContext {

	private User user;
	
	private Collection<? extends GrantedAuthority> authorities;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
}
