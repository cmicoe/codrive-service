package com.cmit.codrive.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
@Entity(name="UserDevice")
@NoArgsConstructor
public class UserDevice extends BaseEntity {

	private static final long serialVersionUID = -254816528915702248L;

	@Column(name="token")
	private String token;
	
	@Column(name="appVersion")
	private String appVersion;
	
	@Column(name="model")
	private String model;
	
	@Column(name="platform")
	private String platform;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="manufacturer")
	private String manufacturer;
	
	@Column(name="osVersion")
	private String osVersion;
	
	@ManyToOne
	@JoinColumn(name="userId")
	private User user;
	
	@Column(name="isVirtual")
	private Boolean isVirtual;
	
	
}
