package com.cmit.codrive.entities;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name="Ride")
@Getter(value = AccessLevel.PUBLIC)
@Setter
@NoArgsConstructor
public class Ride extends BaseEntity {

	private static final long serialVersionUID = 7407634292622994627L;

	@OneToOne
	@JoinColumn(name="routeId")
	private Route route;
	
	@OneToOne
	@JoinColumn(name="rideStatusId")
	private RideStatus rideStatus;
	
	@Column(name="availableSeats")
	private Integer availableSeats;
	
	@Column(name="startDateTime")
	private LocalDateTime startTime;
	
	@Column(name="endDateTime")
	private LocalDateTime endTime;
	
	@OneToMany(mappedBy = "ride", cascade = CascadeType.REMOVE)
	private Set<RidePartner> ridePartners;
	
}
