package com.cmit.codrive.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter(value = AccessLevel.PUBLIC)
@Setter
@NoArgsConstructor
@ToString
@Entity(name="User")
public class User extends BaseEntity {

	private static final long serialVersionUID = 5556385199125130567L;

	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "middleName")
	private String middleName;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "mobile")
	private String mobile;
	
	@Column(name = "isActive")
	private Boolean isActive;
	
	@Column(name= "gender")
	private String gender;
	
	@Column(name = "externalSystemId")
	private String externalSystemId;
	
	@OneToMany(mappedBy = "partner")
	@JsonIgnore
	private Set<RidePartner> partners; 
	
}
