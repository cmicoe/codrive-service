package com.cmit.codrive.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
@Entity(name="RidePartner")
@NoArgsConstructor
public class RidePartner extends BaseEntity {

	private static final long serialVersionUID = -254816528915702248L;

	@ManyToOne
	@JoinColumn(name="rideId")
	private Ride ride;
	
	@ManyToOne
	@JoinColumn(name="partnerId")
	private User partner;
	
	@Column(name="isRider")
	private Boolean isRider;
	
	@ManyToOne
	@JoinColumn(name="pickupLocationId")
	private PickupLocation pickupLocation;
	
	@ManyToOne
	@JoinColumn(name="dropLocationId")
	private PickupLocation dropLocation;
	
	@Column(name="isCancelled")
	private Boolean isCancelled;
	
	@ManyToOne
	@JoinColumn(name="cancelledBy")
	private User cancelledBy;
	
	@Column(name="isCompleted")
	private Boolean isCompleted;
	
}
