package com.cmit.codrive.services.route;

import com.cmit.codrive.services.common.CrudService;
import com.cmit.codrive.valueobjects.RouteVO;

public interface RouteService extends CrudService<RouteVO, Long> {
	
}
