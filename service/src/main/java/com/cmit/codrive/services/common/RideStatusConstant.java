package com.cmit.codrive.services.common;

public enum RideStatusConstant {

	NEW(1),
	ONGOING(2),
	COMPLETED(3),
	CANCELLED(4);
	
	private final Integer value;
	
	private RideStatusConstant(Integer value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return this.name();
	}
	
	public Long getValue() {
		return Long.valueOf(value);
	}
	
}
