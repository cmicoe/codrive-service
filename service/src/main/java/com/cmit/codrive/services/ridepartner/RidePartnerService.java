package com.cmit.codrive.services.ridepartner;

import javax.transaction.Transactional;

import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.services.common.CrudService;
import com.cmit.codrive.valueobjects.RidePartnerVO;

public interface RidePartnerService extends CrudService<RidePartnerVO, Long> {

	@Transactional
	public void cancelRidePartner(UserContext userContext, Long id) throws Exception;
	
	@Transactional
	public void completeRidePartner(UserContext userContext, Long id) throws Exception;
}
