package com.cmit.codrive.services.report;

import java.io.IOException;
import java.time.LocalDate;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.repositories.RideRepository;
import com.cmit.codrive.valueobjects.RideVO;
import com.google.maps.DirectionsApi.RouteRestriction;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;

@Service("rideReportService")
public class RideReportServiceImpl implements RideReportService {

	@Autowired
	RideRepository repository;

	@Autowired
	private ModelMapper mapper;

	@Value("${googleMapsApiKey}")
	private String googleMapsApiKey;

	private GeoApiContext geoApiContext;
	
	@Override
	public Page<RideVO> getCompletedRidesForReport(final UserContext userContext, final LocalDate startDate,
			final LocalDate endDate, int page, int size) throws ApiException, InterruptedException, IOException {

		Page<RideVO> result = repository.findCompletedRideWithinPeriod(startDate, endDate, PageRequest.of(page-1, size)).map((ride) -> {
			return this.mapper.map(ride, RideVO.class);
		});

		
		for (RideVO ride : result) {
			ride.setDistanceCovered(getDistanceCovered(ride));
			ride.getRoute().setPickupLocations(null);
		}

		return result;
	}
	

	private Long getDistanceCovered(final RideVO ride) throws ApiException, InterruptedException, IOException {
		DistanceMatrix response = createDistanceMatrixApiRequest()
				.origins(new LatLng(ride.getRoute().getOriginLattitude(), ride.getRoute().getOriginLongitude()))
				.destinations(new LatLng(ride.getRoute().getDestinationLattitude(),
						ride.getRoute().getDestinationLongitude()))
				.mode(TravelMode.DRIVING).avoid(RouteRestriction.TOLLS).language("en-US").await();
		return response.rows[0].elements[0].distance.inMeters;
	}
	private GeoApiContext getGoogleMapsContext() {
		if (this.geoApiContext == null) {
			this.geoApiContext = new GeoApiContext.Builder().apiKey(googleMapsApiKey).build();
		}
		return this.geoApiContext; 
	}
	
	private DistanceMatrixApiRequest createDistanceMatrixApiRequest() {
		return DistanceMatrixApi.newRequest(getGoogleMapsContext());
	}
}
