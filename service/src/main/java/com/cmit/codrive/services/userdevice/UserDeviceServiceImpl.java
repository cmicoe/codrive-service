package com.cmit.codrive.services.userdevice;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cmit.codrive.entities.User;
import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.entities.UserDevice;
import com.cmit.codrive.repositories.UserDeviceRepository;
import com.cmit.codrive.repositories.UserRepository;
import com.cmit.codrive.services.common.BaseCrudServiceImpl;
import com.cmit.codrive.valueobjects.UserDeviceVO;

@Service("userDeviceService")
@Transactional
public class UserDeviceServiceImpl extends BaseCrudServiceImpl implements UserDeviceService {

	@Autowired
	private UserDeviceRepository repository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper mapper;

	@Override
	public UserDeviceVO createOrUpdateEntity(UserContext userContext, UserDeviceVO entityVO) {

		Optional<UserDevice> existingEntity = this.repository.findByUserIdAndUuid(userContext.getUser().getId(), entityVO.getUuid());

		UserDevice deviceEntity = null;
		
		if (existingEntity.isPresent()) {
			deviceEntity = existingEntity.get();
			deviceEntity.setToken(entityVO.getToken());
			deviceEntity.setAppVersion(entityVO.getAppVersion());
			deviceEntity.setModel(entityVO.getModel());
			deviceEntity.setPlatform(entityVO.getPlatform());
			deviceEntity.setUuid(entityVO.getUuid());
			deviceEntity.setManufacturer(entityVO.getManufacturer());
			deviceEntity.setIsVirtual(entityVO.getIsVirtual());
			deviceEntity.setOsVersion(entityVO.getOsVersion());
			beforeUpdate(userContext, deviceEntity);
		} else {
			deviceEntity = mapper.map(entityVO, UserDevice.class);
			beforeCreate(userContext, deviceEntity);
		}

		final User user = this.userRepository.findById(userContext.getUser().getId()).get();
		deviceEntity.setUser(user);

		return mapper.map(repository.save(deviceEntity), UserDeviceVO.class);

	}

	@Override
	public UserDeviceVO createEntity(UserContext userContext, UserDeviceVO entityVO) {

		final UserDevice entity = mapper.map(entityVO, UserDevice.class);

		beforeCreate(userContext, entity);

		final User user = this.userRepository.findById(userContext.getUser().getId()).get();
		entity.setUser(user);

		return mapper.map(repository.save(entity), UserDeviceVO.class);

	}

	@Override
	public UserDeviceVO updateEntity(UserContext userContext, UserDeviceVO entity) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDeviceVO updateEntity(UserContext userContext, String uuid, String token) throws Exception {
		final UserDevice entity = this.repository.findByUserIdAndUuid(userContext.getUser().getId(), uuid)
				.orElseThrow(EntityNotFoundException::new);

		beforeUpdate(userContext, entity);
		entity.setToken(token);
		return mapper.map(repository.save(entity), UserDeviceVO.class);
	}

	@Override
	public void deleteEntity(UserContext userContext, String uuid) throws Exception {
		UserDevice entity = this.repository.findByUserIdAndUuid(userContext.getUser().getId(), uuid)
				.orElseThrow(EntityNotFoundException::new);
		this.repository.delete(entity);
	}

	@Override
	public void deleteEntity(UserContext userContext, Long id) throws Exception {
		UserDevice entity = this.repository.findById(id).orElseThrow(EntityNotFoundException::new);
		this.repository.delete(entity);

	}

	@Override
	public UserDeviceVO getEntity(UserContext userContext, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserDeviceVO> getEntityList(UserContext userContext) {

		List<UserDeviceVO> filteredResult = this.repository.findByUserId(userContext.getUser().getId()).stream()
				.map((device) -> {
					return this.mapper.map(device, UserDeviceVO.class);
				}).filter(device -> device != null).collect(Collectors.toList());
		return filteredResult;
	}

	@Override
	public Page<UserDeviceVO> getEntityList(UserContext userContext, UserDeviceVO searchFilter, int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

}
