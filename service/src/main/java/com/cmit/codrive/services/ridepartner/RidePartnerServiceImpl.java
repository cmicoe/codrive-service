package com.cmit.codrive.services.ridepartner;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cmit.codrive.entities.PickupLocation;
import com.cmit.codrive.entities.Ride;
import com.cmit.codrive.entities.RidePartner;
import com.cmit.codrive.entities.User;
import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.entities.UserDevice;
import com.cmit.codrive.repositories.PickupLocationRepository;
import com.cmit.codrive.repositories.RidePartnerRepository;
import com.cmit.codrive.repositories.RideRepository;
import com.cmit.codrive.repositories.UserDeviceRepository;
import com.cmit.codrive.repositories.UserRepository;
import com.cmit.codrive.services.common.BaseCrudServiceImpl;
import com.cmit.codrive.services.common.PushNotifications;
import com.cmit.codrive.valueobjects.RidePartnerVO;

@Service("ridePartnerService")
@Transactional
public class RidePartnerServiceImpl extends BaseCrudServiceImpl implements RidePartnerService {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private RideRepository rideRepository;

	@Autowired
	private RidePartnerRepository repository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserDeviceRepository userDevice;

	@Autowired
	private PickupLocationRepository pickupLocationRepository;

	@Autowired
	private PushNotifications pushNotification;

	@Override
	public RidePartnerVO createEntity(UserContext userContext, RidePartnerVO entityVO) throws Exception {
		final RidePartner entity = mapper.map(entityVO, RidePartner.class);

		final Ride ride = this.rideRepository.findUpcomingRideById(entityVO.getRide().getId())
				.orElseThrow(EntityNotFoundException::new);

		final PickupLocation pickupLocation = this.pickupLocationRepository
				.findById(entityVO.getPickupLocation().getId()).orElseThrow(EntityNotFoundException::new);
		
		final PickupLocation dropLocation = this.pickupLocationRepository
				.findById(entityVO.getDropLocation().getId()).orElseThrow(EntityNotFoundException::new);
		
		if (ride.getAvailableSeats() == 0) {
			throw new IllegalArgumentException("No seats left on this ride.");
		}

		// Update the available seats
		ride.setAvailableSeats(ride.getAvailableSeats() - 1);

		final User user = this.userRepository.findById(userContext.getUser().getId())
				.orElseThrow(EntityNotFoundException::new);
		entity.setPartner(user);
		entity.setRide(ride);
		entity.setPickupLocation(pickupLocation);
		entity.setDropLocation(dropLocation);

		beforeCreate(userContext, entity);

		entityVO = mapper.map(repository.save(entity), RidePartnerVO.class);

		this.rideRepository.save(ride);
		
		String message = entity.getPartner().getFirstName() + " joined your ride at " + ride.getStartTime();
		sendNotification(ride, message, "Ride partner Joined");

		return entityVO;
	}

	@Override
	public RidePartnerVO updateEntity(UserContext userContext, RidePartnerVO entity) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteEntity(UserContext userContext, Long id) throws Exception {
		RidePartner entity = this.repository.findById(id).orElseThrow(EntityNotFoundException::new);
		if (entity.getIsRider()) {
			throw new IllegalArgumentException("Rider cannot be deleted");
		}

		this.repository.delete(entity);

		final Ride ride = this.rideRepository.findUpcomingRideById(entity.getRide().getId())
				.orElseThrow(EntityNotFoundException::new);

		// increase the available seats
		ride.setAvailableSeats(ride.getAvailableSeats() + 1);

		this.rideRepository.saveAndFlush(ride);
		
		String message = entity.getPartner().getFirstName() + " has opted out from your ride at " + ride.getStartTime() ;
		sendNotification(ride, message, "Ride partner removed");
	}

	@Override
	public void cancelRidePartner(UserContext userContext, Long id) throws Exception {
		Ride ride = this.rideRepository.findById(id).orElseThrow(EntityNotFoundException::new);
		RidePartner entity = null;
		for (RidePartner ridePartner : ride.getRidePartners()) {
			if (ridePartner.getPartner().getId().equals(userContext.getUser().getId())) {
				entity = ridePartner;
				break;
			}
		}
		if (entity.getIsRider()) {
			throw new IllegalArgumentException("Rider cannot be cancelled");
		}

		if (entity.getIsCancelled()) {
			throw new IllegalArgumentException("Ride is already cancelled for this partner");
		}

		if (entity.getIsCompleted()) {
			throw new IllegalArgumentException(
					"Ride can not be marked as cancelled for this partner. It is already completed");
		}

		entity.setIsCancelled(true);

		final User user = this.userRepository.findById(userContext.getUser().getId())
				.orElseThrow(EntityNotFoundException::new);
		entity.setCancelledBy(user);

		beforeUpdate(userContext, entity);

		this.repository.save(entity);

		// increase the available seats
		ride.setAvailableSeats(ride.getAvailableSeats() + 1);

		this.rideRepository.saveAndFlush(ride);
		String message = entity.getPartner().getFirstName() + " has opted out from your ride at " + ride.getStartTime() ;
		sendNotification(ride, message, "Ride partner removed");
	}

	@Override
	public void completeRidePartner(UserContext userContext, Long id) throws Exception {
		RidePartner entity = this.repository.findById(id).orElseThrow(EntityNotFoundException::new);
		if (entity.getIsRider()) {
			throw new IllegalArgumentException("Rider cannot be marked as complete");
		}

		if (entity.getIsCompleted()) {
			throw new IllegalArgumentException("Ride is already marked as complete for this partner");
		}

		if (entity.getIsCancelled()) {
			throw new IllegalArgumentException(
					"Ride can not be marked as complete for this partner. It is already cancelled.");
		}

		entity.setIsCompleted(true);
		beforeUpdate(userContext, entity);
		this.repository.save(entity);
	}

	@Override
	public RidePartnerVO getEntity(UserContext userContext, Long id) {
		return this.mapper.map(this.repository.findById(id).orElseThrow(EntityNotFoundException::new),
				RidePartnerVO.class);
	}

	@Override
	public List<RidePartnerVO> getEntityList(UserContext userContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<RidePartnerVO> getEntityList(UserContext userContext, RidePartnerVO searchFilter, int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	private void sendNotification(Ride ride, String message, String title) throws Exception {
		for (RidePartner partner : ride.getRidePartners()) {
			if (partner.getIsRider()) {
				List<UserDevice> devices = userDevice.findByUserId(partner.getPartner().getId());
				for (UserDevice device : devices) {
					pushNotification.pushFCMNotification(device.getToken(), title, message);
				}
			}

		}

	}

}
