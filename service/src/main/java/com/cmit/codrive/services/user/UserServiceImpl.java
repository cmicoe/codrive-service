package com.cmit.codrive.services.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cmit.codrive.entities.User;
import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.repositories.UserRepository;
import com.cmit.codrive.services.common.BaseCrudServiceImpl;
import com.cmit.codrive.valueobjects.UserVO;

@Service("userService")
@Transactional
public class UserServiceImpl extends BaseCrudServiceImpl implements UserService {

	@Autowired
	UserRepository repository;
	
	@Autowired
	private ModelMapper mapper;
	
	@Override
	public UserVO createEntity(final UserContext userContext, final UserVO userVO) {
		User entity = null;
		
		
		// check if user already exists
		final Optional<User> optional = this.repository.findByExternalSystemId(userVO.getExternalSystemId());
		this.mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
		if (optional.isPresent()) {
			entity = optional.get();
			this.mapper.map(userVO, entity);
			entity = this.repository.saveAndFlush(entity);
		} else {
			entity = this.mapper.map(userVO, User.class);
			this.repository.save(entity);
		}
		
		return this.mapper.map(entity, UserVO.class);
	}

	@Override
	public UserVO updateEntity(final UserContext userContext, final UserVO userVO) throws Exception {

		User entityToUpdate = this.repository.findById(userVO.getId()).get();
		this.mapper.map(userVO, entityToUpdate);
		return this.mapper.map(repository.save(entityToUpdate), UserVO.class);
	}

	@Override
	public void deleteEntity(final UserContext userContext, final Long id) throws Exception {

		User entityToDelete = this.repository.findById(id).get();
		
		this.repository.delete(entityToDelete);
	}

	@Override
	public UserVO getEntity(UserContext userContext, Long id) {
		
		return this.mapper.map(this.repository.findById(id).get(), UserVO.class);
	}

	@Override
	public List<UserVO> getEntityList(UserContext userContext) {
		List<UserVO> resultList = new ArrayList<>();
		this.repository.findAll().forEach(entity -> {
			resultList.add(this.mapper.map(entity, UserVO.class));
		});

		return resultList;
	}

	@Override
	public Page<UserVO> getEntityList(UserContext userContext, UserVO searchFilter, int page, int size) {
		Page<UserVO> result =	this.repository.findAll(PageRequest.of(page, size)).map((route) -> {
			final UserVO valueObject = this.mapper.map(route, UserVO.class);
			return valueObject;
		});
		return result;
	}

}
