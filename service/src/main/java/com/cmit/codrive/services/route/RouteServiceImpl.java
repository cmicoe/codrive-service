package com.cmit.codrive.services.route;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cmit.codrive.entities.PickupLocation;
import com.cmit.codrive.entities.Route;
import com.cmit.codrive.entities.User;
import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.repositories.PickupLocationRepository;
import com.cmit.codrive.repositories.RouteRepository;
import com.cmit.codrive.repositories.UserRepository;
import com.cmit.codrive.services.common.BaseCrudServiceImpl;
import com.cmit.codrive.valueobjects.RouteVO;

@Service("routeService")
@Transactional
public class RouteServiceImpl extends BaseCrudServiceImpl implements RouteService {

	@Autowired
	private RouteRepository repository;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PickupLocationRepository locationRepository;

	@Autowired
	private ModelMapper mapper;

	@Override
	public RouteVO createEntity(UserContext userContext, RouteVO entityVO) {
		final Route entity = mapper.map(entityVO, Route.class);
		
		beforeCreate(userContext, entity);
		int seqNo = 1;
		
		PickupLocation sourceAsPickupLoc = new PickupLocation();
		sourceAsPickupLoc.setLocation(entity.getOrigin());
		sourceAsPickupLoc.setLattitude(entity.getOriginLattitude());
		sourceAsPickupLoc.setLongitude(entity.getOriginLongitude());
		sourceAsPickupLoc.setSequenceNo(seqNo++);
		beforeCreate(userContext, sourceAsPickupLoc);
		
		
		for (PickupLocation location : entity.getPickupLocations()) {
			beforeCreate(userContext, location);
			location.setSequenceNo(seqNo++);
		}
		
		PickupLocation destAsPickupLoc = new PickupLocation();
		destAsPickupLoc.setLocation(entity.getDestination());
		destAsPickupLoc.setLattitude(entity.getDestinationLattitude());
		destAsPickupLoc.setLongitude(entity.getDestinationLongitude());
		destAsPickupLoc.setSequenceNo(seqNo);
		beforeCreate(userContext, destAsPickupLoc);
		
		entity.getPickupLocations().add(sourceAsPickupLoc);
		entity.getPickupLocations().add(destAsPickupLoc);
		
		final User user = this.userRepository.findById(userContext.getUser().getId()).get();
		entity.setRider(user);
		
		return mapper.map(repository.save(entity), RouteVO.class);
	}

	@Override
	public RouteVO updateEntity(final UserContext userContext, final RouteVO entityVO) throws Exception {

		// map children
		Set<PickupLocation> locations = new HashSet<>();

		entityVO.getPickupLocations().forEach((location) -> {
			if (location.getId() != null) {
				final PickupLocation loc = this.locationRepository.findById(location.getId()).get();
				loc.setModifiedBy(userContext.getUser().getId());
				locations.add(loc);
			} else {
				final PickupLocation loc = this.mapper.map(location, PickupLocation.class);
				loc.setCreatedBy(userContext.getUser().getId());
				loc.setModifiedBy(userContext.getUser().getId());
				locations.add(loc);
			}
		});

		removeDeletedPickupLocations(entityVO);

		Route entityToUpdate = this.mapper.map(entityVO, Route.class);
		entityToUpdate.setModifiedBy(userContext.getUser().getId());
		entityToUpdate.setPickupLocations(locations);
		entityToUpdate = this.repository.saveAndFlush(entityToUpdate);

		final RouteVO result = this.mapper.map(entityToUpdate, RouteVO.class);
		return result;
	}

	private void removeDeletedPickupLocations(RouteVO entityVO) {
		final Route oldRoute = this.repository.findById(entityVO.getId()).get();

		final Set<PickupLocation> oldLoc = oldRoute.getPickupLocations();
		final Set<PickupLocation> newLoc = new HashSet<>();

		entityVO.getPickupLocations().forEach((location) -> {
			newLoc.add(this.mapper.map(location, PickupLocation.class));
		});

		final Set<PickupLocation> union = new HashSet<>(oldLoc);
		union.addAll(newLoc);

		final Set<PickupLocation> intersection = new HashSet<>(oldLoc);
		intersection.retainAll(newLoc);

		union.removeAll(intersection);

		for (final PickupLocation location : union) {
			this.locationRepository.delete(location);
		}

	}

	@Override
	public void deleteEntity(UserContext userContext, Long id) throws Exception {
		Route entityToDelete = repository.findById(id).orElseThrow(EntityNotFoundException::new);

		repository.delete(entityToDelete);
	}

	@Override
	public RouteVO getEntity(UserContext userContext, Long id) {

		return mapper.map(repository.findById(id).orElseThrow(EntityNotFoundException::new), RouteVO.class);
	}

	@Override
	public List<RouteVO> getEntityList(UserContext userContext) {
		List<RouteVO> resultList = new ArrayList<>();
		this.repository.findAll().forEach(entity -> {
			resultList.add(mapper.map(entity, RouteVO.class));
		});
		return resultList;
	}

	@Override
	public Page<RouteVO> getEntityList(UserContext userContext, RouteVO searchFilter, int page, int size) {
		Page<RouteVO> result = repository.findAll(PageRequest.of(page, size)).map((route) -> {
			final RouteVO valueObject = mapper.map(route, RouteVO.class);
			return valueObject;
		});
		return result;
	}

}
