package com.cmit.codrive.services.report;

import java.time.LocalDate;

import org.springframework.data.domain.Page;

import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.valueobjects.RideVO;

public interface RideReportService {
	
	Page<RideVO> getCompletedRidesForReport(final UserContext userContext, final LocalDate startDate, final LocalDate endDate, int page, int size) throws Exception;
	
}
