package com.cmit.codrive.services.ride;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cmit.codrive.entities.Ride;
import com.cmit.codrive.entities.RidePartner;
import com.cmit.codrive.entities.RideStatus;
import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.entities.UserDevice;
import com.cmit.codrive.repositories.RideRepository;
import com.cmit.codrive.repositories.RideStatusRepository;
import com.cmit.codrive.repositories.UserDeviceRepository;
import com.cmit.codrive.services.common.BaseCrudServiceImpl;
import com.cmit.codrive.services.common.PushNotifications;
import com.cmit.codrive.services.common.RideStatusConstant;
import com.cmit.codrive.valueobjects.PickupLocationVO;
import com.cmit.codrive.valueobjects.RideVO;

@Service("rideService")
@Transactional
public class RideServiceImpl extends BaseCrudServiceImpl implements RideService {

	@Autowired
	private RideRepository repository;

	@Autowired
	private RideStatusRepository statusRepository;
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private UserDeviceRepository userDevice;
	
	@Autowired
	private PushNotifications pushNotification;

	@Override
	public RideVO createEntity(final UserContext userContext, RideVO entityVO) {
		final Ride entity = mapper.map(entityVO, Ride.class);
		beforeCreate(userContext, entity);
		
		return  this.mapper.map(repository.save(entity), RideVO.class);
	}

	@Override
	public RideVO updateEntity(final UserContext userContext, RideVO entityVO) throws Exception {
		final Ride entity = this.repository.findById(entityVO.getId()).orElseThrow(EntityNotFoundException::new);
		this.mapper.map(entityVO, entity);

		beforeUpdate(userContext, entity);
		
		return this.mapper.map(this.repository.saveAndFlush(entity), RideVO.class);
	}

	@Override
	public void deleteEntity(final UserContext userContext, Long id) throws Exception {
		Ride entity = this.repository.findById(id).orElseThrow(EntityNotFoundException::new);
		beforeDelete(userContext, entity);
		
		this.repository.delete(entity);
	}

	@Override
	public RideVO getEntity(final UserContext userContext, Long id) {
		return this.mapper.map(this.repository.findById(id).orElseThrow(EntityNotFoundException::new), RideVO.class);
	}

	@Override
	public List<RideVO> getEntityList(final UserContext userContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<RideVO> getEntityList(final UserContext userContext, RideVO searchFilter, int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<RideVO> getUpcomingRidesForUser(final UserContext userContext, @NonNull Integer page, @NonNull Integer size) {

		Page<RideVO> result = this.repository.findAllRidesForUserByStatus(userContext.getUser().getId(),
				RideStatusConstant.NEW.getValue(), PageRequest.of(page-1, size)).map((ride) -> {
					return this.mapper.map(ride, RideVO.class);
				});

		return result;
	}

	@Override
	public Page<RideVO> getCompletedRidesForUser(final UserContext userContext, @NonNull Integer page,
			@NonNull Integer size) {
		Page<RideVO> result = this.repository.findAllRidesForUserByStatus(userContext.getUser().getId(),
				RideStatusConstant.COMPLETED.getValue(), PageRequest.of(page-1, size)).map((ride) -> {
					return this.mapper.map(ride, RideVO.class);
				});
		return result;
	}

	@Override
	public Page<RideVO> searchRides(UserContext userContext, LocalDateTime startDate, LocalDateTime endDate,
			String source, String destination, List<String> daysOfWeek,	Integer page, Integer size) {
		Page<RideVO> result = this.repository.searchRides(startDate, endDate, source, destination,
				daysOfWeek, userContext.getUser().getId(), PageRequest.of(page-1, size))
				.map((ride)->{
					return this.mapper.map(ride, RideVO.class);			
				});
		
		List<RideVO> filteredResult = result.getContent().stream().map((ride) -> {
			int sourceSeqNo = 0, destinationSeqNo = 0;
			for (PickupLocationVO pickupLocation : ride.getRoute().getPickupLocations()) {
				if (pickupLocation.getLocation().equals(source)) {
					sourceSeqNo = pickupLocation.getSequenceNo().intValue();
				} else if (pickupLocation.getLocation().equals(destination)) {
					destinationSeqNo = pickupLocation.getSequenceNo().intValue();
				}
			}
			if (sourceSeqNo < destinationSeqNo || (sourceSeqNo == 0 && destinationSeqNo == 0)) {
				return this.mapper.map(ride, RideVO.class);							
			}
			return null;
		}).filter(ride-> ride != null).collect(Collectors.toList());
		
		Page<RideVO> res = new PageImpl<>(filteredResult, result.getPageable(), result.getTotalElements());
		
		return res;
	}
	
	@Override
	public void cancelRide(UserContext userContext, Long id) throws Exception {
		
		Ride entity = this.repository.findById(id).orElseThrow(EntityNotFoundException::new);

		if (entity.getRideStatus().getId() != RideStatusConstant.NEW.getValue()) {
			throw new IllegalStateException("Only rider can cancel the ride.");
		}
		
		// Only rider can cancel the ride
		if (!checkIfRider(entity, userContext)) {
			throw new IllegalArgumentException("Only rider can cancel the ride.");
		}
				
		RideStatus status = this.statusRepository.findByCode(RideStatusConstant.CANCELLED.toString()).orElseThrow(EntityNotFoundException::new);
		entity.setRideStatus(status);
		
		this.repository.saveAndFlush(entity);
		String message = "Your ride at " + entity.getStartTime() + " is cancelled by "  + userContext.getUser().getFirstName();
		sendNotification(entity , message, "Ride cancelled");
	}

	@Override
	public void completeRide(UserContext userContext, Long id) throws Exception {
		
		Ride entity = this.repository.findById(id).orElseThrow(EntityNotFoundException::new);
		
		if (entity.getRideStatus().getId() != RideStatusConstant.NEW.getValue()) {
			throw new IllegalStateException("Only rider can complete the ride.");
		}
		
		// Only rider can complete the ride
		if (!checkIfRider(entity, userContext)) {
			throw new IllegalArgumentException("Only rider can complete the ride.");
		}
		
		
		RideStatus status = this.statusRepository.findByCode(RideStatusConstant.COMPLETED.toString()).orElseThrow(EntityNotFoundException::new);
		entity.setRideStatus(status);
		entity.setEndTime(LocalDateTime.now());
		
		this.repository.saveAndFlush(entity);
		String message = "Your ride at " + entity.getStartTime() + " is marked completed by " + userContext.getUser().getFirstName();
		sendNotification(entity , message, "Ride completed");
	}
	
	private void sendNotification(Ride entity, String message, String title) throws Exception {
		for (RidePartner ridePartner : entity.getRidePartners()) {
			List<UserDevice> devices = userDevice.findByUserId(ridePartner.getPartner().getId());
			for (UserDevice device : devices) {
				pushNotification.pushFCMNotification(device.getToken(), title, message);
			}
		}
		
	}

	private Boolean checkIfRider(final Ride ride, final UserContext userContext) {
		for (RidePartner partner: ride.getRidePartners()) {
			if (partner.getIsRider() && partner.getPartner().getId() != userContext.getUser().getId()) {
				return false;
			}
		}
		
		return true;
	}
	

}
