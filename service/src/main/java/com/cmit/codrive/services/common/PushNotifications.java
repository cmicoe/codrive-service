package com.cmit.codrive.services.common;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *  Utility to send Notifications from server to client end.
 *
 */
@Component
public class PushNotifications { 
	
	@Value("${authKey}")
	private String authKey;
	
	@Value("${fmcUrl}")
	private String fmcUrl;
	
	/**
	 * @param userDeviceFirebaseToken the device firebase token to send the notification to.
	 * @param title the notification message
	 * @param message the notification message
	 * @throws Exception
	 */
	public void pushFCMNotification(String userDeviceFirebaseToken, String title, String message) throws Exception { 
		
	URL url = new URL(fmcUrl);
	HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	conn.setUseCaches(false);
	conn.setDoInput(true);
	conn.setDoOutput(true);

	conn.setRequestMethod("POST");
	conn.setRequestProperty("Authorization","key="+authKey);
	conn.setRequestProperty("Content-Type","application/json");

	JSONObject json = new JSONObject();
	json.put("to",userDeviceFirebaseToken);
	JSONObject data = new JSONObject();
	data.put("content-available", 0);
	data.put("title", title.trim());
	data.put("body",message.trim());
	json.put("data", data);
	
	JSONObject info = new JSONObject();
	info.put("title", title.trim());   
	info.put("body",message.trim());
	info.put("sound", "new_alarm_notification");
	info.put("content-available", Boolean.TRUE);
	info.put("collapse_key","0");
	json.put("notification", info);
	OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	wr.write(json.toString());
	wr.flush();
	conn.getInputStream();
	}
	
}
