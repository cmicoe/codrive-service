package com.cmit.codrive.services.userdevice;

import javax.transaction.Transactional;

import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.services.common.CrudService;
import com.cmit.codrive.valueobjects.UserDeviceVO;

public interface UserDeviceService extends CrudService<UserDeviceVO, Long> {

	@Transactional
	public UserDeviceVO createOrUpdateEntity(UserContext userContext, UserDeviceVO entityVO) throws Exception;
	
	@Transactional
	public UserDeviceVO updateEntity(UserContext userContext, String uuid, String token) throws Exception;
	
	@Transactional
	public void deleteEntity(UserContext userContext, String uuid) throws Exception;
}
