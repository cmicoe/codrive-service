package com.cmit.codrive.services.common;

import com.cmit.codrive.entities.BaseEntity;
import com.cmit.codrive.entities.UserContext;

public abstract class BaseCrudServiceImpl {

	protected void beforeCreate(final UserContext userContext, final BaseEntity entity) {
		entity.setCreatedBy(userContext.getUser().getId());
		entity.setModifiedBy(userContext.getUser().getId());
	}
	
	protected void beforeUpdate(final UserContext userContext, final BaseEntity entity) {
		entity.setModifiedBy(userContext.getUser().getId());
	}
	
	protected void beforeDelete(final UserContext userContext, final BaseEntity entity) {
		entity.setModifiedBy(userContext.getUser().getId());
	}
}
