package com.cmit.codrive.services.ride;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.domain.Page;

import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.services.common.CrudService;
import com.cmit.codrive.valueobjects.RideVO;

public interface RideService extends CrudService<RideVO, Long> {
	Page<RideVO> getUpcomingRidesForUser(UserContext userContext, Integer page, Integer size);
	
	Page<RideVO> getCompletedRidesForUser(UserContext userContext, Integer page, Integer size);
	
	Page<RideVO> searchRides(UserContext userContext, final LocalDateTime startDate, final LocalDateTime endDate, 
							final String source, final String destination, final List<String> daysOfWeek,
							Integer page, Integer size);

	void cancelRide(UserContext userContext, Long id) throws Exception;

	void completeRide(UserContext userContext, Long id) throws Exception;
}
